package com.librarysystem.servlet.manager;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.librarysystem.entity.BorrowBook;
import com.librarysystem.entity.Manager;
import com.librarysystem.entity.Reader;
import com.librarysystem.service.BbService;
import com.librarysystem.service.BookService;
import com.librarysystem.service.ManagerService;
import com.librarysystem.service.ReaderService;

/**
 * Servlet implementation class DoBookcx
 */
@WebServlet("/dbcx")
public class DoBookcx extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoBookcx() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		request.setCharacterEncoding("utf-8");
		int librarycardnum=Integer.parseInt(request.getParameter("librarycardnum")) ;
	    BorrowBook b=new BorrowBook();
	   BbService bs=new BbService();
	    List<BorrowBook> list=bs.findBorrowBook(librarycardnum);
	    request.setAttribute("list", list);
//	    System.out.println("6666");
		request.getRequestDispatcher("/WEB-INF/jsp/bookcx.jsp").forward(request, response);
	
	}

}
