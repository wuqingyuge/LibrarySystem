package com.librarysystem.service;

import java.util.List;

import com.librarysystem.dao.BorrowBookDao;
import com.librarysystem.entity.Books;
import com.librarysystem.entity.BorrowBook;;

public class BbService {
	BorrowBookDao bbd=new BorrowBookDao();
	public int addBorrowBook(BorrowBook bbook){
		int count=bbd.addBorrowBook(bbook);
		return count;
	}
	  public List<BorrowBook> findBorrowBook(int librarycardnum){
   		return bbd.findBorrowBook(librarycardnum);		
   	}
}
