package com.librarysystem.dao;

import java.util.ArrayList;
import java.util.List;

import com.librarysystem.entity.BorrowBook;
import com.librarysystem.entity.Manager;
import com.librarysystem.util.DBUtil;

public class BorrowBookDao extends BaseDao{
	public int addBorrowBook(BorrowBook bbook){
		int count=0;
		try{
			connection = DBUtil.getConnection();
			preparedStatement=connection.prepareStatement("insert into borrowbook(bid,librarycardnum,borrowtime,borrowdaynum) values(?,?,?,?)");
			preparedStatement.setInt(1,bbook.getBid());
			preparedStatement.setInt(2,bbook.getLibrarycardnum());
			preparedStatement.setString(3,bbook.getBorrowtime() );
			preparedStatement.setInt(4,bbook.getBorrowdaynum());
			count=preparedStatement.executeUpdate();
			
		}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				
		    }finally{
		    	closed();
		    }
		return count;

			}	
	public List<BorrowBook> findBorrowBook(int librarycardnum){
		List<BorrowBook> list = new ArrayList<>();
		try{
			connection = DBUtil.getConnection();
			preparedStatement=connection.prepareStatement("select * from borrowbook where librarycardnum=?");
			preparedStatement.setInt(1,librarycardnum);
			resultSet=preparedStatement.executeQuery();
			BorrowBook b= null;
			while(resultSet.next()){
				b= new BorrowBook();
				b.setId(resultSet.getInt("id"));
				 b.setBid(resultSet.getInt("bid"));
				b.setLibrarycardnum(resultSet.getInt("librarycardnum"));
				b.setBorrowtime(resultSet.getString("borrowtime"));
				 list.add(b);
			}
			return list;
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
		    }
	}
}
